////////////////////////////////
// This program is the intellectual property of
//     NICHOLAS J EGGLESTON
// Created for CS284 - Operating Systems
//   Professor: Sriram Chellappan
// Assignment: Assignment 4 - Virtual Memory Management System 
//
//Filename: main.cpp

#include <iostream>
#include <fstream>
#include <unistd.h>

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <vector>

using namespace std;

struct page {           
  int address;              // Memory location of the page.
  bool useBit;              // Used by Clock to determine replacement order.
  unsigned long timeStamp;  // Used by LRU and FIFO to determine replacement order.
};

// Generic Functions.
  bool isMultipleOf2(int x);
  bool validPageSys(string);              // Checks input for valid paging system.
  unsigned int getNumFrames();            // Gets the possible number of frames.
  unsigned int getMaxPagesPerProg();      // Gets the maximum number of pages per program.
  unsigned int getMemoryLocation(unsigned int, unsigned int); // Gets the memory location of a page.
  bool isInMainMemory(int, unsigned int); // Checks to see if a page is already in that program's memory.
  bool isFreeSpace(int);                  // Checks to see if a program has free space in it's memory.
  
// Paging functions.
  void pr_lru(bool, int, int);    // Least Recently Used Replacement.
  void pr_fifo(bool, int, int);   // First In First Out Replacement.
  void pr_clock(bool, int, int);  // Clock Policy Replacement.

// Global Constants.
  unsigned int MAINMEMORYSIZE = 512;  // Change this to change overall main memory of simulated system.

// Global Variables.
  unsigned long age = 1;        // Used for memory accesses.
  unsigned int pageSize = 0;    // Holds the size of a page.
  unsigned long numFaults = 0;  // Stores the number of page faults.
  fstream progList;             // File stream for the Program List.
  fstream progTrace;            // File stream for the Program Trace.

vector<vector<page> > mainMemory;  // Each Program can have n number of frames... mainMemory[numProgs][numframes]
vector<int> programs; // Contains the initial page reference for each program.
vector<int> clkPtrs;  // Holds the clock pointer for each program.

int main(int argc, char *argv[]) {
  system("clear");
  cout << "\tNick's Memory Management Simulator" << endl;
  
  if(argc != 6) { // argc should be 6 for correct execution.
    cout << "Silly human, you must provide me with exactly 6, "
         << "arguments on the command line, you have provided: "
         << argc << "\n   I will now exit, please try again." <<endl;
    exit(1);
  }
  
  else { // Number of arguments is right.
    // Store program arguments.
    string list = argv[1], trace = argv[2], pageSystem = argv[4];
    pageSize = atoi(argv[3]);
    bool pagingType = atoi(argv[5]);
    
    if(isMultipleOf2(pageSize)) { // Valid page size.
      if(validPageSys(pageSystem)) { // Valid paging system.
        progList.open(list.c_str());   // Open program list.
        if(!progList) { // Bad list file.
          cerr << "Could not open Program List." << endl;
          cout << "Memory Management Simulator will now close." << endl << endl;
          exit(2);
        }
        progTrace.open(trace.c_str());  // Open program trace.
        if(!progTrace) {  // Bad trace file.
          cerr << "Could not open Program Trace." << endl;
          cout << "Memory Management Simulator will now close." << endl << endl;
          exit(3);
        }
        
        // Print inputs for user friendliness.
          cout << "Inputs from user:" << endl;
          cout << "\tProgram List:  " << list << endl
               << "\tProgram Trace: " << trace << endl
               << "\tPage Size:     " << pageSize << endl
               << "\tPaging System: " << pageSystem << endl
               << "\tPaging Type:   ";
          if(pagingType)
            cout << "PrePaging";
          else
            cout << "Demand Paging";
          cout << endl;
        
        // Step 1: Declaring all of the variables and constants.
          // We already kinda sort have done this and will do this shortly...
        // Step 2: Read in the program list file.
          programs.clear();
          while(!progList.eof()) { // Read through the entire Program List.
            int program, memReq;
            progList >> program >> memReq;
            memReq = memReq/pageSize;
            programs.push_back(memReq); // Hold onto initial page.
          }

        // Step 3: Load the initial memory.
          for(unsigned int i = 0; i < programs.size(); i++) { // For every program.
            vector<page> tempVP;
            mainMemory.push_back(tempVP); // Push in a temporary vector to initialize.
            for(unsigned int j = 0; j < getMaxPagesPerProg(); j++) { // Initialize all possible pages per program.
              page tempP;
              
              if(j == 0) { // If it's the first element of a program's memory.
                tempP.address = programs[i];  // Record the first page.
                clkPtrs.push_back(0);         // Add corresponding number of clock positions for each program.
              }
              else
                tempP.address = -1; // This is the key to identify empty elements.

              tempP.useBit = 1;
              tempP.timeStamp = age++;
              mainMemory[i].push_back(tempP); // Load the initialized element.
            }
          }
          
        // Step 4: Perform actions recorded in Program Trace.
          int prog = 0, loc = 0;
          while(!progTrace.eof()) { // Read through the entire Program Trace.
            progTrace >> prog >> loc; // Get instructions.
            loc  = loc/pageSize;
            if(isInMainMemory(prog, loc)) { // Desired address is already in main memory.
              if(pageSystem == "lru" || pageSystem == "LRU") { // We need to update it's timestamp for LRU.
                for(int x = 0; x < mainMemory.size(); x++) {
                  if(mainMemory[prog][x].address == loc)
                    mainMemory[prog][x].timeStamp = age;  // Update it's age since it was referenced.
                }
              }
              else if(pageSystem == "clock" || pageSystem == "Clock" || pageSystem == "CLOCK") {
               for(int x = 0; x < mainMemory.size(); x++) {
                  if(mainMemory[prog][x].address == loc)
                    mainMemory[prog][x].useBit = 0; // Clear it's useBit since it was referenced.
                }
              }
            }
            else { // Desired address is not in main memory, we need to do get it there.
              programs[prog] = loc;
              if(isFreeSpace(prog)) { // There's free space!  Add it in an open spot.
                bool hasInserted = false;
                for(unsigned int x = 0; x < mainMemory[prog].size(); x++) { // For every spot in that program's memory.
                  if(!hasInserted && mainMemory[prog][x].address == -1) { // Hasn't inserted yet and is free slot.
                    page tempP;
                    tempP.address = loc;
                    tempP.useBit = 1;
                    tempP.timeStamp = age;
                    mainMemory[prog][x] = tempP;  // Loads the requested page.
                    hasInserted = true;
                  }
                }
              }
              else { // There's no free space, we need to replace a page.
                if(pageSystem == "clock" || pageSystem == "Clock" || pageSystem == "CLOCK")
                  pr_clock(pagingType, prog, programs[prog]); // Clock Policy Replacement.
                else if(pageSystem == "lru" || pageSystem == "LRU")
                  pr_lru(pagingType, prog, programs[prog]);   // Least Recently Used Replacement.
                else if(pageSystem == "fifo" || pageSystem == "FIFO")
                  pr_fifo(pagingType, prog, programs[prog]);  // First In First Out Replacement.
              }
              numFaults++;  // Increment the number of page faults since... well... we page faulted.
            }
            // else -> We don't have to do anything since it's already in main memory.
            if(!pagingType) // Demand Paging.
              age++;        // We added one page.
            else            // Prepaging.
              age += 2;     // We added two pages, so we need to up the age by 2.
          }
        
        cout << endl << "Number of page faults: " << numFaults << endl << endl;
        cout << "Memory Management Simulator will now end." << endl << endl << endl;
        
        progList.close();
        progTrace.close();
      }
      else {  // Invalid paging system.
        cerr << "Invalid Paging System Input." << endl;
        cout << "Memory Management Simulator will now end." << endl << endl;
        exit(4);
      }
    }
    else { // Invalid page size... quit.
      cerr << "Invalid Page Size Input." << endl;
      cout << "Memory Management Simulator will now end." << endl << endl;
      exit(5);
    }
  }
  
  return 0;
}


bool isMultipleOf2(int x) { // Checks to see if parameter is a multiple of 2.
  return ((((x&&!(x&(x-!!x)))+x)-x)^x)^x
}

bool validPageSys(string alg) { // Checks to see if the input Paging System is valid.
  bool isGood;
  if(alg == "clock" || alg == "lru" || alg == "fifo")
    isGood = true;
  else
    isGood = false;

  return isGood;
}

unsigned int getNumFrames() { // Returns the number of frames in the sytem.
  return MAINMEMORYSIZE/pageSize;
}

unsigned int getMaxPagesPerProg() { // Returns the maximum number of pages per program.
  return (MAINMEMORYSIZE/pageSize)/programs.size();
}

unsigned int getMemoryLocation(unsigned int memNum) { // Returns the memory location of a page.
  return memNum/pageSize;
}

bool isInMainMemory(int prog, unsigned int pageNum) { // Checks to see if a page is already in that program's main memory.
  for(unsigned int q = 0; q < mainMemory[prog].size(); q++)
    if(mainMemory[prog][q].address == pageNum)
      return true;

  return false;
}

bool isFreeSpace(int prog) { // Checks to see if a program has free space in it's main memory.
  for(unsigned int x = 0; x < mainMemory[prog].size(); x++)
    if(mainMemory[prog][x].address == -1)
      return true;

  return false;
}

void pr_clock(bool type, int prog, int pageNum) { // Clock Policy Replacement Alrogrithm.
  bool replaced = false;
  int x = clkPtrs[prog];
  // Demand Paging.  Loads the page requested and the next page.
    while(!replaced) {  // While we haven't replaced it.
      for(x; x < mainMemory[prog].size(); x++) { // Go through each slot.
        if(!replaced && mainMemory[prog][x].useBit == 0) { // We can replace this one.  Moves Clock Pointer.
          page tempP;
          tempP.address = pageNum;
          tempP.useBit = 1;
          tempP.timeStamp = age;
          mainMemory[prog][x] = tempP;  // Replace the page.
          replaced = true;
        }
        else
          mainMemory[prog][x].useBit = 0; // Clear use bit.
      }
      if(!replaced) // We haven't replaced yet!  Force roll over of Clock Pointer.
        x = 0;
    }
    clkPtrs[prog] = x+1;  // Move the Clock Pointer to the next element.
  
  if(type) { // Prepaging.  Load the next page too.  Process is the same as before.
    replaced = false;
    while(!replaced) {
      for(x; x < mainMemory[prog].size(); x++) {
        if(!replaced && mainMemory[prog][x].useBit == 0) { // We can replace this one.
          page tempP;
          tempP.address = pageNum;
          tempP.useBit = 1;
          tempP.timeStamp = age+1;
          mainMemory[prog][x] = tempP;
          replaced = true;
        }
        else
          mainMemory[prog][x].useBit = 0;
      }

      if(!replaced) // Force roll over.
        x = 0;
    }
    clkPtrs[prog] = x+1;
  }
}

void pr_lru(bool type, int prog, int pageNum) { // Least Recently Used Replacement Algorithm.
  int oldestX = prog, oldestY = 0;
  page tempP;

  for(unsigned int y = 0; y < mainMemory[prog].size(); y++) { // For every page already in that program's memory.
    if(mainMemory[prog][y].timeStamp < mainMemory[oldestX][oldestY].timeStamp &&
       mainMemory[prog][y].timeStamp < age)  // Is older than previous oldest.
       // Save these coordinates.
      oldestY = y;
  }

  // Demand Paging.  Loads only the page requested.
    tempP.address = pageNum;
    tempP.useBit = 1;
    tempP.timeStamp = age;
    mainMemory[oldestX][oldestY] = tempP; // Replace the page.

  if(type) { // Prepaging.  Loads the page requested and the next page.
    // Load the second page.   
      for(unsigned int y = 0; y < mainMemory[prog].size(); y++) { // For every page already in that program's memory.
        if(mainMemory[prog][y].timeStamp < mainMemory[oldestX][oldestY].timeStamp &&  // Is older than previous oldest.
           mainMemory[prog][y].timeStamp < age)
        { // Save these coordinates.
          oldestY = y;
        }
      }
    
    tempP.address = pageNum+1;
    tempP.useBit = 1;
    tempP.timeStamp = age+1;
    mainMemory[oldestX][oldestY] = tempP; // Replace the page.
  }
    // Load the second page.   
      for(unsigned int y = 0; y < mainMemory[prog].size(); y++) { // For every page already in that program's memory.
        if(mainMemory[prog][y].timeStamp < mainMemory[oldestX][oldestY].timeStamp &&  // Is older than previous oldest.
           mainMemory[prog][y].timeStamp < age)
          // Save these coordinates.
          oldestY = y;
        
      }
    
    tempP.address = pageNum+1;
    tempP.useBit = 1;
    tempP.timeStamp = age+1;
    mainMemory[oldestX][oldestY] = tempP;   // Replace the page.
}

void pr_fifo(bool type, int prog, int pageNum) { // First In First Out Replacement Algorithm.
  int oldestX = prog, oldestY = 0;
  page tempP;

  for(unsigned int y = 0; y < mainMemory[prog].size(); y++) { // For every page already in that program's memory.
    if(mainMemory[prog][y].timeStamp < mainMemory[oldestX][oldestY].timeStamp &&
       mainMemory[prog][y].timeStamp < age)  // Is older than previous oldest.
      // Save these coordinates.
      oldestY = y;
  }

  // Demand Paging.  Loads only the page requested.
    tempP.address = pageNum;
    tempP.useBit = 1;
    tempP.timeStamp = age;
    mainMemory[oldestX][oldestY] = tempP; // Replace the page.

  if(type) { // Prepaging.  Loads the page requested and the next page.
    // Load the second page.   
      for(unsigned int y = 0; y < mainMemory[prog].size(); y++) { // For every page already in that program's memory.
        if(mainMemory[prog][y].timeStamp < mainMemory[oldestX][oldestY].timeStamp &&  // Is older than previous oldest.
           mainMemory[prog][y].timeStamp < age)
          // Save these coordinates.
          oldestY = y;
      }
    
    tempP.address = pageNum+1;
    tempP.useBit = 1;
    tempP.timeStamp = age+1;
    mainMemory[oldestX][oldestY] = tempP; // Replace the page.
  }
}

